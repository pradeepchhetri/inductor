(ns inductor.core
  (:require [clj-http.client :as http-client]
            [cheshire.core   :as json])
  (import [java.net URLEncoder]))

;;
;; Client options
;;

(def default-client
  "Default HTTP client configuration."
  { :base-uri   "https://api.indix.com/v1"
    :app-id     "foobar"
    :app-key    "foobar" })

(defn make-client
  "Returns a map representing an HTTP client configuration.

  Valid options:
    :base-uri   (default: \"https://api.indix.com/v1\")
    :app-id     (default: \"foobar\")
    :app-key    (default: \"foobar\")"
  [opts]
  (merge default-client opts))

;;
;; Reads the JSON response and get keywords back.
;;

(defn read-result
  [r]
  (json/parse-string (r :body)))

;;
;; HTTP URL generation
;;

(defn gen-url-fn
  {:no-doc true}
  [client action]
  (str
    (client :base-uri)
    (cond
      (= (action :action) :search-store) (str "/stores/?query=" (action :query) "&")
      (= (action :action) :search-brand) (str "/brands/?query=" (action :query) "&")
      (= (action :action) :list-category) (str "/categories/?")
      (= (action :action) :search-product) (str "/products/?query=" (action :query) "&")
      (= (action :action) :product-detail) (str "/products/" (action :product-id) "?")
      (= (action :action) :product-offers) (str "/products/" (action :product-id) "?view=offers&")
      (= (action :action) :catalog-offers) (str "/products/" (action :product-id) "?view=offers_catalog&")
      (= (action :action) :catalog) (str "/products/" (action :product-id) "?view=catalog&")
      (= (action :action) :store-offers) (str "/products/" (action :product-id) "/offers?storeId=" (action :store-id) "&")
      (= (action :action) :store-prices) (str "/products/" (action :product-id) "/prices?storeId=" (action :store-id) "&"))
    "app_id="
    (client :app-id)
    "&app_key="
    (client :app-key)))


(defmulti gen-url-multi
  (fn [_ action] (class action)))

(defmethod gen-url-multi clojure.lang.Keyword
  [client action]
  (gen-url-fn client { :action action }))

(defmethod gen-url-multi clojure.lang.PersistentArrayMap
  [client action]
  (gen-url-fn client action))

(def gen-url
  (memoize gen-url-multi))

;;
;; Search store name.
;;

(defn search-store-req
  "List all stores matching the query string."
  [client store]
  (let [url (gen-url client { :action :search-store
                              :query store })]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn search-store
  "List all stores matching the query string."
  [client store]
  (read-result (search-store-req client store)))

;;
;; Search brand name.
;;

(defn search-brand-req
  "List all brands matching the query string."
  [client brand]
  (let [url (gen-url client { :action :search-brand
                              :query brand })]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn search-brand
  "List all brands matching the query string."
  [client brand]
  (read-result (search-brand-req client brand)))

;;
;; List categories.
;;

(defn list-category-req
  "List all categories available."
  [client]
  (let [url (gen-url client :list-category)]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn list-category
  "List all categories available."
  [client]
  (read-result (list-category-req client)))


;;
;; Search Product.
;;

(defn search-product-req
  "List all products matching the query string."
  [client product]
  (let [url (gen-url client { :action :search-product
                              :query product })]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn search-product
  "List all products matching the query string."
  [client product]
  (read-result (search-product-req client product)))

;;
;; Product Detail.
;;

(defn product-detail-req
  "List all the details of the product matching the product-id."
  [client product-id]
  (let [url (gen-url client { :action :product-detail
                              :product-id product-id })]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn product-detail
  "List all the details of the product matching the product-id."
  [client product-id]
  (read-result (product-detail-req client product-id)))

;;
;; Product Offers.
;;

(defn product-offers-req
  "List all the available offers of the product matching the product-id."
  [client product-id]
  (let [url (gen-url client { :action :product-offers
                              :product-id product-id })]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn product-offers
  "List all the available offers of the product matching the product-id."
  [client product-id]
  (read-result (product-offers-req client product-id)))

;;
;; Catalog Offers.
;;

(defn catalog-offers-req
  "List all the available catalog offers of the product matching the product-id."
  [client product-id]
  (let [url (gen-url client { :action :catalog-offers
                              :product-id product-id})]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn catalog-offers
  "List all the available catalog offers of the product matching the product-id."
  [client product-id]
  (read-result (catalog-offers-req client product-id)))


;;
;; Catalog
;;

(defn catalog-req
  "List the catalog of the product matching the product-id."
  [client product-id]
  (let [url (gen-url client { :action :catalog
                              :product-id product-id})]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn catalog
  "List the catalog of the product matching the product-id."
  [client product-id]
  (read-result (catalog-req client product-id)))

;;
;; Store Offers
;;

(defn store-offers-req
  "List all the offers for a store."
  [client product-id store-id]
  (let [url (gen-url client { :action :store-offers
                              :product-id product-id
                              :store-id store-id })]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn store-offers
  "List all the offers for a store."
  [client product-id store-id]
  (read-result (store-offers-req client product-id store-id)))

;;
;; Store Prices
;;

(defn store-prices-req
  "List all the prices for a store."
  [client product-id store-id]
  (let [url (gen-url client { :action :store-prices
                              :product-id product-id
                              :store-id store-id })]
    (http-client/get url {
      :socket-timeout        1000   ;; in milliseconds
      :conn-timeout          1000   ;; in milliseconds
      :accept                :json
      :throw-entire-message? true })))

(defn store-prices
  "List all the prices for a store."
  [client product-id store-id]
  (read-result (store-prices-req client product-id store-id)))
