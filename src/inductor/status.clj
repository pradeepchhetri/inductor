(ns inductor.status)

(defn success?
  [^long status]
  (<= 200 status 299))

(defn redirect?
  [^long status]
  (<= 300 status 399))

(defn unauthorized?
  [^long status]
  (= 401 status))

(defn unavailable?
  [^long status]
  (= 402 status))

(defn forbidden?
  [^long status]
  (= 403 status))

(defn missing?
  [^long status]
  (= 404 status))

(defn throttling?
  [^long status]
  (= 429 status))

(defn client-error?
  [^long status]
  (<= 400 status 499))

(defn server-error?
  [^long status]
  (<= 500 status 599))

