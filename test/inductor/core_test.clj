(ns inductor.core-test
  (:require [clojure.test :refer :all]
            [inductor.core :refer :all]))

(deftest test-client-00
  (testing "Default"
    (is (=
         { :base-uri   "https://api.indix.com/v1"
           :app-id     "foobar"
           :app-key    "foobar"
         }
         (make-client {})))))

(deftest test-client-01
  (testing "Custom"
    (is (=
         { :base-uri   "https://api.indix.com/v1"
           :app-id     "barfoo"
           :app-key    "barfoo"
         }
         (make-client {
           :base-uri   "https://api.indix.com/v1"
           :app-id     "barfoo"
           :app-key    "barfoo" })))))

(deftest test-gen-url-00
  (testing "search-store"
    (is (= "https://api.indix.com/v1/stores/?query=wal&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :search-store
                                       :query "wal" })))))

(deftest test-gen-url-01
  (testing "search-brand"
    (is (= "https://api.indix.com/v1/brands/?query=wal&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :search-brand
                                       :query "wal" })))))

(deftest test-gen-url-02
  (testing "list-category"
    (is (= "https://api.indix.com/v1/categories/?app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) :list-category )))))

(deftest test-gen-url-03
  (testing "search-product"
    (is (= "https://api.indix.com/v1/products/?query=nike&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :search-product
                                       :query "nike" })))))

(deftest test-gen-url-04
  (testing "product-detail"
    (is (= "https://api.indix.com/v1/products/4444?app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :product-detail
                                       :product-id "4444" })))))

(deftest test-gen-url-05
  (testing "product-offers"
    (is (= "https://api.indix.com/v1/products/4444?view=offers&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :product-offers
                                       :product-id "4444" })))))

(deftest test-gen-url-06
  (testing "catalog-offers"
    (is (= "https://api.indix.com/v1/products/4444?view=offers_catalog&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :catalog-offers
                                       :product-id "4444" })))))

(deftest test-gen-url-07
  (testing "catalog"
    (is (= "https://api.indix.com/v1/products/4444?view=catalog&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :catalog
                                       :product-id "4444" })))))

(deftest test-gen-url-08
  (testing "store-offers"
    (is (= "https://api.indix.com/v1/products/4444/offers?storeId=264&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :store-offers
                                       :product-id "4444"
                                       :store-id   "264"  })))))

(deftest test-gen-url-09
  (testing "store-prices"
    (is (= "https://api.indix.com/v1/products/4444/prices?storeId=264&app_id=foobar&app_key=foobar"
           (gen-url (make-client {}) { :action :store-prices
                                       :product-id "4444"
                                       :store-id   "264"  })))))


