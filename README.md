Inductor
=========

A Clojure Client for [Indix API](https://developer.indix.com).

Installation
------------

### Leiningen

**Not yet published to Clojars**

Inductor is distributed via [Clojars](https://clojars.org/inductor). Add the
following to your dependencies in `project.clj`:

```clj
:dependencies [[inductor "0.1.0-SNAPSHOT"]]
```

Usage
-----

### Require in your app

```clj
(require '[inductor.core :as client])
```

### Configure a client

```clj
(def client
  (client/make-client { :app-id "foobar"  :app-key "foobar" }))
```

The `default-client` options are:

```clj
{ :base-uri   "https://api.indix.com/v1"
  :app-id     "foobar"
  :app-key    "foobar" }
```

### Search for a store

```clj
(client/search-store client "wal")
```

This will parse the json response and return keywords back on which you can do all kind of operations.

### Search for a brand

```clj
(client/search-brand client "wal")
```


## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
