(defproject inductor "0.1.0-SNAPSHOT"
  :description "A Clojure Client for Indix API"
  :url "https://developer.indix.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
    [org.clojure/clojure "1.5.1"]
    [clj-http            "0.9.1"]
    [cheshire            "5.3.1"]
  ]
  :plugins [
    [codox           "0.6.6"]
    [lein-marginalia "0.7.1"]
  ]
  :dev-dependencies [
    [codox "0.6.6"]
  ]
  :aot :all)
